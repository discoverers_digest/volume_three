# Volume III

A single call `latexmk main` should now do the job, without having to have any fonts installed locally (now shipped with this repo).
The tool `latexmk` should be available in all regular distributions.

