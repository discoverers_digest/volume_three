\begin{figure*}[h]                                                           
  \includegraphics[width=\linewidth]{./media/images/doors}%
%  \scriptsize{\textsc{\\This is} the article main image caption.}
  \label{fig:doors}%                                                 
\end{figure*}                                                                
\begin{quotation} 
\noindent\color{Sepia}{{\textit{\textbf{“Inside of every problem lies an opportunity.”}}}}\\[.5mm]
%remove following line space if you're tight on vertical room and need to fit on
%single page

\hfill\color{Sepia}{\small{\textendash \textsc{Robert Kiposaki}}}
\end{quotation}
\pagebreak
\marginnote{\href{http://ifdb.tads.org/showuser?id=nufzrftl37o9rw5t}{By Brian Rushton}}[2em]
\lettrine[lines=3]{\color{BrickRed}E}{\enspace very year}, voters select one puzzle in one game to receive the \textsc{xyzzy} Best Individual Puzzle Award. These puzzles capture the community’s fancy in some way, through complexity, inventiveness, or even emotion. Of all the \textsc{xyzzy} categories, this award seemed to me like a grab-bag of random games at first, with little connection between different winners.

However, I was determined to find out what they had in common. After carefully playing through all the winning puzzles, I was surprised to find that they fell into a few broad categories. I’d like to discuss these (while avoiding spoilers as much as possible). 

The main categories are Learning a System, Iterative/Babel Fish Puzzles, and Perspective Shift. These categories have some overlap; the Babel Fish puzzles are essentially a blend of the other two categories.

\section{Learning a System}

Roughly half of the best puzzles presented a complex system, sometimes taking up the entire game, which you must learn through experimentation. These puzzles are hard to spoil — your tools and goals are clear from the outset, and the main difficulty is figuring out which tools do what.

The earliest Best Puzzle recipient in this category is the language puzzle in The Edifice (1997), where the player meets an NPC who communicates entirely in a language of the author’s invention. Players must communicate with the NPC and ask specific questions by experimenting with the words they hear.

Another language puzzle appeared in 2001 with The Gostak, a polarizing game that received the IFComp Golden Banana of Discord for having the highest standard deviation in votes. This game is written in a pseudo-English, based on a sentence that says ‘The Gostak distims the doshes’. As in The Edifice, the player must experiment with new words and their responses to learn how to play the game.

The Best Puzzles in 2004 and 2014 involved time travel. All Things Devours (2004) and Fifteen Minutes (2014) are both one-puzzle games with a similar premise: you have run out of time, you have access to a time machine, and more than one version of yourself is required to get the job done. The two games differ in how they handle the multiple versions of yourself. All Things Devours has a large map, and the difficulty in the game is learning how to avoid your copies in order to prevent a paradox from occurring. Fifteen Minutes is a one-room game that relishes having all the copies present and interacting simultaneously. Both games are best approached with copious note-taking materials.

The last two System-Based Best Puzzles are found in Delightful Wallpaper (2006) and An Act of Murder (2007). The first game, by Andrew Plotkin, prevents the player from manipulating anything directly. However, the house they explore reacts to their movement, with movement in one part of the house triggering doors and gates on the other side of the house. The game provides a helpful note-taking system, but it is fiendishly difficult to map out a path that will open the doors you need. The second game, An Act of Murder, is a randomized murder mystery — it also requires careful note-taking to establish alibis and determine motive.

What do these system games have in common? For one, they don’t overwhelm the player. Other attempts at language puzzles dump big dictionaries or convoluted grammar on the player at the start. The Gostak and The Edifice present a sentence or a paragraph and let you go from there. Other time travel puzzles (and there are several of them) can force you to interact with copies of yourself too early.

Conversely, these Best Puzzles also grow complex enough for the player to develop a true feeling of mastery. The Gostak requires a complicated sequence of actions, and the player must make a complicated request in The Edifice. The maze in Delightful Wallpaper was quite large, and the time travel puzzle in Fifteen Minutes can have you interacting with more than half a dozen copies of yourself.

Finally, these puzzles clearly establish the function of each new piece. Delightful Wallpaper will actually write down what you’ve learned at each stage, making it completely unambiguous. The use of each new word in The Gostak or The Edifice is made clear by the many situations it appears in, and so on.

So it appears that people appreciate complicated systems that begin with basic principles and build to a complicated finale. This is the same as learning a system in real life, from mathematics to physics to cooking to speaking a language. These puzzles train you to apply a useful skill (in-game), and they train you well.

\section{Iterative or Babel Fish Puzzles}

The term Babel Fish puzzle comes from the Infocom game Hitchhiker’s Guide to the Galaxy (1984), where the player tried to get a babel fish from a vending machine. When the player fixes the apparent problem preventing them from getting the fish, a new problem is introduced — the fish slips through grates and gets vacuumed up by cleaning robots, but each new failure is only introduced after solving the earlier problems. The term is now used to describe puzzles with an iterative approach, where each new try yields new information.

Puzzles in this category include the games of Rematch (2000) and Lock \& Key (2002), and the sequence in Violet (2008) that disconnects the internet. Some of these could be classified as Learning the System or Perspective Shift puzzles, but I feel they belong here.

Much of the charm in Violet lies more in the writing than in the puzzle structure. Each step makes you laugh at your own lack of self-control, and the counterintuitive actions required for the final solution may color your perception of the central character for the rest of the game.

In Rematch and Lock \& Key, the player sets up a system, sees how it works, and restarts. Rematch is largely about getting a single, complicated command correct — the author warns you that the final command is so long that the parser had to be partially rewritten to accept it. Gameplay consists of iteratively experimenting with lengthier commands until the correct one is found. 

Lock \& Key has you designing a dungeon from about 16 pre-made rooms before watching an adventurer break through them. Like Rematch, it’s clear what each part of the puzzle does, but assembling them in the right order and the right length is difficult. Rematch keeps the puzzle interesting with a super-dramatic recurring event, and Lock \& Key keeps it interesting with hilarious descriptions.

Interestingly, the writing and atmosphere of these puzzles seem to be more responsible for their success than the mechanics of the puzzles themselves. The frequent failures required to advance are shoved in your face, and they are only palatable because of the amusing or compelling messages that you have earned. A Babel fish puzzle without good writing is only an exercise in frustration.

\section{Perspective Shift Puzzles}

These puzzles are difficult to discuss without spoiling, so I’ll talk about them in general terms. They are puzzles where an author has presented the solution, or the puzzle itself, in a deliberately confusing way. You need a sudden flash of insight, usually gained by changing your perspective, to understand the puzzle.

Many authors have tried to write compelling Perspective Shift puzzles and failed. The Best Individual Puzzle winners share a few ingredients to success:
\begin{itemize}[leftmargin=0em]
\item{A constrained environment. In each of these games, the puzzle is solved after almost all other puzzles are out of play, when the player is in a small area and focused on the one puzzle; it is clear that something special is required to move onward.}
\item{The solution involves actions, or clues, that have been introduced earlier. In every case — except perhaps one — an earlier puzzle required you to perform the exact same action for a different purpose. This establishes that the solution is actually possible within the game.}
\item{It involves using something familiar in an unfamiliar way, which is what makes the puzzle interesting.}
\item{Strong dramatic tension adds a sense of urgency to solving the puzzle. This is often the threat of death.}
\end{itemize}
I don’t want to spoil any actual examples, so I’ll use one from literature: in The Fellowship of the Ring, Gandalf and the fellowship must solve a puzzle to open the gates of Moria. Hostile enemies and impassable terrain have closed off their other options. They are in a narrow, dead-end valley that has a lake on one side and the gates on another. 

The inscription on the gates of Moria is originally read as a greeting: “Speak, friend, and enter.” The fellowship spends hours working to identify what needs to be spoken while Gandalf speaks word after word of power.

Finally, with the help of the hobbits, Gandalf laughs and realizes that the inscription is a set of instructions: "Speak 'friend,' and enter.” They speak the word “friend” and open the gates just as a monster attacks.

The characters were in a constrained situation with no other distractions; they had a compelling, story-based need to go in; and the solution was unconventional. The classic idea of a “password” was turned on its head — it was a word that everyone should know, instead of a word that no-one should know. 

The solution was part of the inscription, and there were earlier hints in the book. Much was made of the former friendship between dwarves and elves; the solution supported the earlier information that had been supplied about the old days.

\section{The last puzzle: Four Hats}

The 2011 Best Puzzle is different from the others. Four of the 2011 IFComp games included a recurring character who was looking for his hat. Most people did not notice, or they chalked it up to coincidence (like the year when several games featured a giant squid). 

The four games were quite different: Cold Iron (by an anonymous Plotkin) was a short, mostly puzzle-less story about a world where faeries may or may not exist; Playing Games was a sequence of three mazes made in ASCII art, with a threadbare story tying them together; the Last Day of Summer was a short atmospheric piece about regrets in a village; and the Life and Deaths of Doctor M was a full-blown journey through the afterlife, exploring a doctor’s questionable actions through flashbacks.

All of them had a general setting of late-1800’s to early 1900’s, based on architecture and clothing (for Doctor M, in the afterlife section only), along with a feeling of remembrance and wistfulness. And, of course, the recurring theme of the hat.

The puzzle required information to be transferred between the various games. One game might have something like an opened lock displaying a combination, and the next game has the same combination lock, but closed. One game describes a society where no one talks to you unless you bow three times, and the next game includes that society but doesn’t tell you to bow. You can only see the entire puzzle by playing all 4 games.

I believe that this puzzle won for originality and cleverness; it was a bit unfair in practice. Nobody even deduced its existence during the competition. However, it does have some things in common with the other Best Puzzles: there is a real sense of learning and mastery as you proceed from game to game, because you are applying secret knowledge that is unavailable in the game world. You also use items in unfamiliar ways, providing new perspectives on previously mundane materials. In any case, this is certainly the most unusual of the Best Individual Puzzles.

\section{Conclusion}

While the Best Individual Puzzles are a mixed bag, voters have exhibited some consistent trends. Apart from the hat puzzle, all of the puzzles are fair: either the rules are clearly explained, or the puzzle is set up in a way that allows more than half of the players to stumble onto the solution. All of the puzzles teach the player something, whether a new system or a new way of looking at the game world. 

Each puzzle was also embedded in a polished, bug-free, and frequently well-written game.

What does this mean for authors? It’s hard to come up with specific recommendations, but many authors seem to neglect the idea of fairness. The number of unfair puzzles in games that provide misleading error messages, or provide huge info dumps all at once, or neglect to give any hint as to your purpose, or require you to “guess the verb,” is almost uncountable. 

Ultimately, leading players to a puzzle’s solution seems to require as much
craft as technically implementing the puzzle itself.

\bigskip
\marginnote{\href{http://ifdb.tads.org/showuser?id=nufzrftl37o9rw5t}{Brian
    Rushton's Interactive Fiction Database page}}[1em]
\begin{wrapfigure}{l}{0.25\textwidth}
  \vspace{-3em}
  \begin{center}
  \includegraphics[width=\linewidth]{./media/images/brian}%
%  \scriptsize{\textsc{\\This is} the article main image caption.}
  \label{fig:brian}%
  \end{center}
\end{wrapfigure}                                                                
\noindent\emph{Brian Rushton is a mathematician with an avid interest in interactive fiction.
He currently lives in Washington State with his wife and son, and enjoys stories in every
form. He is also a frequent contributor to the math articles on Wikipedia under
the name \emph{brirush}}.
