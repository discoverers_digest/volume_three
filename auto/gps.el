(TeX-add-style-hook
 "gps"
 (lambda ()
   (LaTeX-add-labels
    "fig:multiple_paths"
    "fig:gps_location"
    "fig:new_text_game"
    "fig:new_web_game"
    "fig:if_cover"
    "fig:indie"
    "sec:coordinates"
    "fig:lat_lon"
    "fig:facade"
    "sec:testing"
    "fig:about_config"
    "fig:switched"))
 :latex)

